const fs = require('fs');
const os = require('os');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question(`Choose an option:
            1. Read package.json
            2. Display OS info
            3. start HTTP server
            Type a number:
            `, (answer) => {


    if (parseInt(answer) === 1){
        readPackageJson()
    } else if (parseInt(answer) === 2) {
        displayOSInfo()
    } else if (parseInt(answer) === 3) {
        startServer()
    } else {
        console.log("Invalid option");
    }

  rl.close();
});



const displayOSInfo = () => {
    console.log("SYSTEM MEMORY:", os.totalmem() / 1024 / 1024 / 1024)
    console.log("FREE MEMORY:", os.freemem() / 1024 / 1024 / 1024)
    console.log("ARCH:", os.arch());
    console.log("PLATFORM:", os.platform());
    console.log("USER:", os.userInfo()['username']);
    console.log("RELEASE:", os.release());
    console.log("CPU CORES:", os.cpus().length);
}

const readPackageJson = () => {
    fs.readFile('package.json', 'utf8', (err,data) => {
        console.log(data);
    })
}

const startServer = () => {
    const express = require('express')
    const app = express()
    const { PORT = 3000 } = process.env
    const path = require('path')

    app.listen(PORT, ()=>{
        console.log(`Server started on port ${PORT}...`)
    })

    app.get('/', (req,res) => {
        return res.sendFile(path.join(__dirname,'client','index.html'))
    })


    app.get('/contact', (req,res) => {
        return res.sendFile(path.join(__dirname,'client','contact.html'))
    })

    // middleware
    app.use(express.static('client'))
    app.use(express.json())
}