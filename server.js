const express = require('express')
const app = express()
const { PORT = 3000 } = process.env
const path = require('path')

app.listen(PORT, ()=>{
    console.log(`Server started on port ${PORT}...`)
})

app.get('/', (req,res) => {
    return res.sendFile(path.join(__dirname,'client','index.html'))
})


app.get('/contact', (req,res) => {
    return res.sendFile(path.join(__dirname,'client','contact.html'))
})

// middleware
app.use(express.static('client'))
app.use(express.json())

